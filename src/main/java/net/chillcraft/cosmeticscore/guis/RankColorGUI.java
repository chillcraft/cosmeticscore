package net.chillcraft.cosmeticscore.guis;

import net.chillcraft.cosmeticscore.util.DyedItemBuilder;
import net.chillcraft.cosmeticscore.util.ItemBuilder;
import net.chillcraft.cosmeticscore.CosmeticsCore;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class RankColorGUI implements CommandExecutor{


    public CosmeticsCore plugin;

    public RankColorGUI(CosmeticsCore plugin) { this.plugin = plugin; }


    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
            final Player player = (Player) sender;
            if (cmd.getName().equalsIgnoreCase("rankcolor") && player.hasPermission("cosmeticscore.platinum")){
                RankColorInv(plugin, player);
            }
        return true;
    }


    public static void RankColorInv (final JavaPlugin plugin, final Player player){

        final Inventory RankColor = Bukkit.createInventory(null, 27, CosmeticsCore.color("&8Choose your rank color..."));

            ItemStack RedRank = new DyedItemBuilder(Material.WOOL, DyeColor.RED)
                    .setName(CosmeticsCore.color("&cRed Rank Color"))
                    .setLore(CosmeticsCore.color("Change your Platinum title to &8[&c&lPlatinum&8]"))
                    .result();
            ItemStack GoldRank = new DyedItemBuilder(Material.WOOL, DyeColor.ORANGE)
                    .setName(CosmeticsCore.color("&6Gold Rank Color"))
                    .setLore(CosmeticsCore.color("Change your Platinum title to &8[&6&lPlatinum&8]"))
                    .result();
            ItemStack YellowRank = new DyedItemBuilder(Material.WOOL, DyeColor.YELLOW)
                    .setName(CosmeticsCore.color("&eYellow Rank Color"))
                    .setLore(CosmeticsCore.color("Change your Platinum title to &8[&e&lPlatinum&8]"))
                    .result();
            ItemStack LightBlueRank = new DyedItemBuilder(Material.WOOL, DyeColor.LIGHT_BLUE)
                    .setName(CosmeticsCore.color("&bLight Blue Rank Color"))
                    .setLore(CosmeticsCore.color("Change your Platinum title to &8[&c&lPlatinum&8]"))
                    .result();
            ItemStack PinkRank = new DyedItemBuilder(Material.WOOL, DyeColor.PINK)
                    .setName(CosmeticsCore.color("&dPink Rank Color"))
                    .setLore(CosmeticsCore.color("Change your Platinum title to &8[&c&lPlatinum&8]"))
                    .result();
            ItemStack ResetRankColor = new ItemBuilder(Material.BARRIER)
                    .setName(CosmeticsCore.color("&c&lReset Rank Color"))
                    .setLore(CosmeticsCore.color("Reset your Platinum title back to &8[&7&lPlatinum&8]"))
                    .result();
            RankColor.setItem(10, RedRank);
            RankColor.setItem(11, GoldRank);
            RankColor.setItem(12, YellowRank);
            RankColor.setItem(13, LightBlueRank);
            RankColor.setItem(14, PinkRank);
            RankColor.setItem(16, ResetRankColor);
            player.openInventory(RankColor);


        }

    }
