package net.chillcraft.cosmeticscore.guis;

import net.chillcraft.cosmeticscore.CosmeticsCore;
import net.chillcraft.cosmeticscore.util.DyedItemBuilder;
import net.chillcraft.cosmeticscore.util.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class NameColorGUI {

    public static void NameColorInventory (final JavaPlugin plugin, final Player player){
        FileConfiguration config = plugin.getConfig();
        final Inventory NameColorInv = Bukkit.createInventory(null, 54, CosmeticsCore.color("&8Choose your name color..."));

        //Member Colors

        ItemStack MemberColorHolder = new DyedItemBuilder(Material.STAINED_GLASS_PANE, DyeColor.WHITE)
                .setName(CosmeticsCore.color("&fMember Unlocked Colors"))
                .setLore(" ", CosmeticsCore.color("&7These colors are unlocked by joining the server!"))
                .result();
        ItemStack MemberColorWhite = new DyedItemBuilder(Material.WOOL, DyeColor.WHITE)
                .setName(CosmeticsCore.color("&fWhite Name Color"))
                .setLore(" ", CosmeticsCore.color("&7Click to change to &fthis &7chat color!"))
                .result();
        ItemStack MemberColorGray = new DyedItemBuilder(Material.WOOL, DyeColor.GRAY)
                .setName(CosmeticsCore.color("&7Gray Name Color"))
                .setLore(" ", CosmeticsCore.color("&7Click to change to this chat color!"))
                .result();
        NameColorInv.setItem(10, MemberColorHolder);
        NameColorInv.setItem(11, MemberColorWhite);
        NameColorInv.setItem(12, MemberColorGray);

        //Donor Colors

        ItemStack DonorColorHolder = new DyedItemBuilder(Material.STAINED_GLASS_PANE, DyeColor.MAGENTA)
                .setName(CosmeticsCore.color("&dDonor Unlocked Colors"))
                .setLore(" ", CosmeticsCore.color("&7These name colors are unlocked by purchasing a donor rank!"))
                .result();
        ItemStack DonorColorPink = new DyedItemBuilder(Material.WOOL, DyeColor.PINK)
                .setName(CosmeticsCore.color("&dPink Name Color"))
                .setLore(" ", CosmeticsCore.color("&6Requires Premium Rank"), " ", CosmeticsCore.color("&7Click to change to &dthis&7 chat color!"))
                .result();
        ItemStack DonorColorLightGreen = new DyedItemBuilder(Material.WOOL, DyeColor.LIME)
                .setName(CosmeticsCore.color("&aLight Green Name Color"))
                .setLore("", CosmeticsCore.color("&dRequires Deluxe Rank"), "", CosmeticsCore.color("&7Click to change to &athis&7 chat color!"))
                .result();
        ItemStack DonorColorLightBlue = new DyedItemBuilder(Material.WOOL, DyeColor.LIGHT_BLUE)
                .setName(CosmeticsCore.color("&bLight Blue Name Color"))
                .setLore("", CosmeticsCore.color("&7&lRequires Platinum Rank"), "", CosmeticsCore.color("&7Click to change to &bthis&7 chat color!"))
                .result();
        ItemStack DonorColorYellow = new DyedItemBuilder(Material.WOOL, DyeColor.YELLOW)
                .setName(CosmeticsCore.color("&eYellow Name Color"))
                .setLore("", CosmeticsCore.color("&7&lRequires Platinum Rank"), "", CosmeticsCore.color("&7Click to change to &ethis&7 chat color!"))
                .result();
        NameColorInv.setItem(19, DonorColorHolder);
        NameColorInv.setItem(20, DonorColorPink);
        NameColorInv.setItem(21, DonorColorLightGreen);
        NameColorInv.setItem(22, DonorColorLightBlue);
        NameColorInv.setItem(23, DonorColorYellow);

        //Staff Colors

        ItemStack StaffColorHolder = new DyedItemBuilder(Material.STAINED_GLASS_PANE, DyeColor.RED)
                .setName(CosmeticsCore.color("&cStaff Unlocked Colors"))
                .setLore("", CosmeticsCore.color("&7These name colors are unlocked by purchasing a donor rank!"))
                .result();
        ItemStack StaffColorOrange = new DyedItemBuilder(Material.WOOL, DyeColor.ORANGE)
                .setName(CosmeticsCore.color("&6Gold Name Color"))
                .setLore("", CosmeticsCore.color("&7Click to change to &6this&7 chat color!"))
                .result();
        ItemStack StaffColorRed = new DyedItemBuilder(Material.WOOL, DyeColor.RED)
                .setName(CosmeticsCore.color("&cRed Name Color"))
                .setLore("", CosmeticsCore.color("&7Click to change to &cthis&7 chat color!"))
                .result();
        ItemStack ChatColorExit = new ItemBuilder(Material.BARRIER)
                .setName(CosmeticsCore.color("&c&lBack"))
                .setLore("", CosmeticsCore.color("&bClick here to go back to the main menu!"))
                .result();
        NameColorInv.setItem(28, StaffColorHolder);
        NameColorInv.setItem(29, StaffColorOrange);
        NameColorInv.setItem(30, StaffColorRed);
        NameColorInv.setItem(49, ChatColorExit);
        player.openInventory(NameColorInv);

    }

}
