package net.chillcraft.cosmeticscore.guis;

import net.chillcraft.cosmeticscore.util.DyedItemBuilder;
import net.chillcraft.cosmeticscore.CosmeticsCore;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class PerksGUI {


    public static void OpenGUI(final JavaPlugin plugin, final Player player){
        FileConfiguration config = plugin.getConfig();
        final Inventory PerksMenu = Bukkit.createInventory(null, 27, CosmeticsCore.color("&8Donor Rank Perks"));

        List<String> PremLore = config.getStringList("PremiumPerks");
        List<String> PremLoreC = new ArrayList<>();
        for (String string : PremLore) PremLoreC.add(CosmeticsCore.color(string));
        ItemStack PremiumPerks = new DyedItemBuilder(Material.STAINED_GLASS, DyeColor.ORANGE)
                .setLore(PremLoreC)
                .setName(CosmeticsCore.color("&6Premium &e(&a$15.00&e)"))
                .result();

        List<String> DeluxeLore = config.getStringList("DeluxePerks");
        List<String> DeluxeLoreC = new ArrayList<>();
        for (String string : DeluxeLore) DeluxeLoreC.add(CosmeticsCore.color(string));
        ItemStack DeluxePerks = new DyedItemBuilder(Material.STAINED_GLASS, DyeColor.PURPLE)
                .setLore(DeluxeLoreC)
                .setName(CosmeticsCore.color("&5Deluxe &d(&a$25.00&d)"))
                .result();

        List<String> PlatinumLore = config.getStringList("PlatinumPerks");
        List<String> PlatinumLoreC = new ArrayList<>();
        for (String string : PlatinumLore) PlatinumLoreC.add(CosmeticsCore.color(string));
        ItemStack PlatinumPerks = new DyedItemBuilder(Material.STAINED_GLASS, DyeColor.GRAY)
                .setLore(PlatinumLoreC)
                .setName(CosmeticsCore.color("&7&lPlatinum &f(&a$50.00&f)"))
                .result();

        PerksMenu.setItem(11, PremiumPerks);
        PerksMenu.setItem(13, DeluxePerks);
        PerksMenu.setItem(15, PlatinumPerks);
        player.openInventory(PerksMenu);
    }
}
