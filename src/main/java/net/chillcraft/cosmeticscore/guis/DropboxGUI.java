package net.chillcraft.cosmeticscore.guis;

import net.chillcraft.cosmeticscore.util.ItemBuilder;
import net.chillcraft.cosmeticscore.CosmeticsCore;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

import static org.bukkit.ChatColor.RED;
import static org.bukkit.ChatColor.YELLOW;

public class DropboxGUI {

    public CosmeticsCore plugin;

    public DropboxGUI(CosmeticsCore plugin) {
        this.plugin = plugin;
    }



    public static void openMainGUI(final JavaPlugin plugin, final Player player) {
        String GUIName = plugin.getConfig().getString("GUIName");
        final Inventory dropboxmenu = Bukkit.createInventory(null, 9, ChatColor.translateAlternateColorCodes('&', "&3&lDropbox"));
        FileConfiguration config = plugin.getConfig();

        //Dropbox Information
        ItemStack info = new ItemStack(Material.EMPTY_MAP);
        ItemMeta metainfo = info.getItemMeta();
        List<String> infolore = config.getStringList("DropboxInfo");
        List<String> coloredilore = new ArrayList<>();
        for (String string : infolore) { coloredilore.add(CosmeticsCore.color(string)); }
        metainfo.setLore(coloredilore);
        metainfo.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&7&lInformation"));
        info.setItemMeta(metainfo);
        dropboxmenu.setItem(0, info);

        //May 2018 Dropbox
        ItemStack May2018Dropbox = new ItemBuilder(Material.ENDER_CHEST)
                .setName(CosmeticsCore.color("&b&lMay 2018 Dropbox"))
                .setLore(" ", CosmeticsCore.color("&f&l* &6[&f*****&6] &7Tag"), CosmeticsCore.color("&f&l* &a[&2Cactus&a] &7Tag"), CosmeticsCore.color("&f&l* &eRainbow Helix Particle Effect"), ("&f&l* &eSell Wand Booster - 2x 30 Minutes"))
                .result();
        dropboxmenu.setItem(1, May2018Dropbox);

        player.openInventory(dropboxmenu);
    }

    /*
    May 2018 DropBox
       Contains:
           - Cactus Tag
           - 5 Star Tag
           - Rainbow Helix Particle Effect
           - Sell Wand Booster - 2x 30 Minutes
    */
    public static void Dropbox1(final Player player) {
        final Inventory dropbox1 = Bukkit.createInventory(null, 9, CosmeticsCore.color("&b&lMay 2018 Dropbox"));


        //Cactus Tag
        ItemStack May2018CactusTag = new ItemBuilder(Material.NAME_TAG)
                .setName(CosmeticsCore.color("&a[&2Cactus&a] &7Tag"))
                .setLore(" ", YELLOW + "Click to apply your tag!")
                .result();
        dropbox1.setItem(0, May2018CactusTag);
        //Star Tag
        ItemStack May2018StarTag = new ItemBuilder(Material.NAME_TAG)
                .setName(CosmeticsCore.color("&6[&f*****&6] &7Tag"))
                .setLore(" ", YELLOW + "Click to apply your tag!")
                .result();
        dropbox1.setItem(1, May2018StarTag);
        //Particle
        ItemStack May2018Particle = new ItemBuilder(Material.DOUBLE_PLANT)
                .setName(CosmeticsCore.color("&6Rainbow Helix"))
                .setLore(" ", YELLOW + "Click to open Dropbox Particles!")
                .result();
        dropbox1.setItem(2, May2018Particle);
        //Booster
        ItemStack May2018Booster = new ItemBuilder(Material.DIAMOND)
                .setName(CosmeticsCore.color("&6Sell Wand Booster - 2x 30 Minutes"))
                .setLore(" ", YELLOW + "Click to view your unused boosters!")
                .result();
        dropbox1.setItem(3, May2018Booster);
        //Exit
        ItemStack May2018Exit = new ItemBuilder(Material.BARRIER)
                .setName(RED + "Back")
                .setLore(" ", RED + "Click to go back to the main menu!")
                .result();
        dropbox1.setItem(8, May2018Exit);

        player.openInventory(dropbox1);
        /*
        June 2018 Dropbox
            Contains:

        */

    }
}
