package net.chillcraft.cosmeticscore.guis;

import net.chillcraft.cosmeticscore.util.DyedItemBuilder;
import net.chillcraft.cosmeticscore.util.ItemBuilder;
import net.chillcraft.cosmeticscore.CosmeticsCore;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class ColorGUI {


    public static void OpenMainGUI(final JavaPlugin plugin, final Player player) {
        FileConfiguration config = plugin.getConfig();

        Inventory ColorGUI = Bukkit.createInventory(null, 27, CosmeticsCore.color("&7/color"));
        ItemStack RankColor = new ItemBuilder(Material.NETHER_STAR)
                .setName(CosmeticsCore.color("&b&lRank Color"))
                .setLore("", CosmeticsCore.color("&c&lRequires Platinum Rank"), "", CosmeticsCore.color("&7Click here to change your rank color!"))
                .result();
        ItemStack NameColor = new ItemBuilder(Material.NAME_TAG)
                .setName(CosmeticsCore.color("&6&lName Color"))
                .setLore("", CosmeticsCore.color("&7Click here to change your name color!"))
                .result();
        ItemStack IslandLevelColor = new ItemBuilder(Material.GRASS)
                .setName(CosmeticsCore.color("&e&lIsland Level Color"))
                .setLore("", CosmeticsCore.color("&7Click here to change your island level color!"))
                .result();
        ItemStack ChatColor = new ItemBuilder(Material.GREEN_RECORD)
                .setName(CosmeticsCore.color("&a&lChat Color"))
                .setLore("", CosmeticsCore.color("&7Click here to change your chat color!"))
                .result();
        ColorGUI.setItem(10, RankColor);
        ColorGUI.setItem(12, NameColor);
        ColorGUI.setItem(14, IslandLevelColor);
        ColorGUI.setItem(16, ChatColor);
        player.openInventory(ColorGUI);
    }


    public static void OpenRankColorGUI(final JavaPlugin plugin, final Player player){
        RankColorGUI.RankColorInv(plugin, player);
    }
    public static void OpenNameColorGUI(final JavaPlugin plugin, final Player player){
    NameColorGUI.NameColorInventory(plugin, player);
    }

    public static void OpenIslandLevelColorGUI(final JavaPlugin plugin, final Player player){

        Inventory IslandLevelColorGUI = Bukkit.createInventory(null, 54, CosmeticsCore.color("Choose your island level color..."));

        player.openInventory(IslandLevelColorGUI);
    }

    public static void OpenChatColorGUI(final JavaPlugin plugin, final Player player) {

        Inventory ChatColorGUI = Bukkit.createInventory(null, 54, CosmeticsCore.color("Choose your chat color..."));

        //Member Colors

        ItemStack MemberColorHolder = new DyedItemBuilder(Material.STAINED_GLASS_PANE, DyeColor.WHITE)
                .setName(CosmeticsCore.color("&fMember Unlocked Colors"))
                .setLore(" ", CosmeticsCore.color("&7These colors are unlocked by joining the server!"))
                .result();
        ItemStack MemberColorWhite = new DyedItemBuilder(Material.WOOL, DyeColor.WHITE)
                .setName(CosmeticsCore.color("&fWhite Chat Color"))
                .setLore(" ", CosmeticsCore.color("&7Click to change to &fthis &7chat color!"))
                .result();
        ItemStack MemberColorGray = new DyedItemBuilder(Material.WOOL, DyeColor.GRAY)
                .setName(CosmeticsCore.color("&7Gray Chat Color"))
                .setLore(" ", CosmeticsCore.color("&7Click to change to this chat color!"))
                .result();
        ChatColorGUI.setItem(10, MemberColorHolder);
        ChatColorGUI.setItem(11, MemberColorWhite);
        ChatColorGUI.setItem(12, MemberColorGray);

        //Donor Colors

        ItemStack DonorColorHolder = new DyedItemBuilder(Material.STAINED_GLASS_PANE, DyeColor.MAGENTA)
                .setName(CosmeticsCore.color("&dDonor Unlocked Colors"))
                .setLore(" ", CosmeticsCore.color("&7These colors are unlocked by purchasing a donor rank!"))
                .result();
        ItemStack DonorColorPink = new DyedItemBuilder(Material.WOOL, DyeColor.PINK)
                .setName(CosmeticsCore.color("&dPink Chat Color"))
                .setLore(" ", CosmeticsCore.color("&6Requires Premium Rank"), " ", CosmeticsCore.color("&7Click to change to &dthis&7 chat color!"))
                .result();
        ItemStack DonorColorLightGreen = new DyedItemBuilder(Material.WOOL, DyeColor.LIME)
                .setName(CosmeticsCore.color("&aLight Green Chat Color"))
                .setLore("", CosmeticsCore.color("&dRequires Deluxe Rank"), "", CosmeticsCore.color("&7Click to change to &athis&7 chat color!"))
                .result();
        ItemStack DonorColorLightBlue = new DyedItemBuilder(Material.WOOL, DyeColor.LIGHT_BLUE)
                .setName(CosmeticsCore.color("&bLight Blue Chat Color"))
                .setLore("", CosmeticsCore.color("&7&lRequires Platinum Rank"), "", CosmeticsCore.color("&7Click to change to &bthis&7 chat color!"))
                .result();
        ItemStack DonorColorYellow = new DyedItemBuilder(Material.WOOL, DyeColor.YELLOW)
                .setName(CosmeticsCore.color("&eYellow Chat Color"))
                .setLore("", CosmeticsCore.color("&7&lRequires Platinum Rank"), "", CosmeticsCore.color("&7Click to change to &ethis&7 chat color!"))
                .result();
        ChatColorGUI.setItem(19, DonorColorHolder);
        ChatColorGUI.setItem(20, DonorColorPink);
        ChatColorGUI.setItem(21, DonorColorLightGreen);
        ChatColorGUI.setItem(22, DonorColorLightBlue);
        ChatColorGUI.setItem(23, DonorColorYellow);

        //Staff Colors

        ItemStack StaffColorHolder = new DyedItemBuilder(Material.STAINED_GLASS_PANE, DyeColor.RED)
                .setName(CosmeticsCore.color("&cStaff Unlocked Colors"))
                .setLore("", CosmeticsCore.color("&7These colors are unlocked by purchasing a donor rank!"))
                .result();
        ItemStack StaffColorOrange = new DyedItemBuilder(Material.WOOL, DyeColor.ORANGE)
                .setName(CosmeticsCore.color("&6Gold Chat Color"))
                .setLore("", CosmeticsCore.color("&7Click to change to &6this&7 chat color!"))
                .result();
        ItemStack StaffColorRed = new DyedItemBuilder(Material.WOOL, DyeColor.RED)
                .setName(CosmeticsCore.color("&cRed Chat Color"))
                .setLore("", CosmeticsCore.color("&7Click to change to &cthis&7 chat color!"))
                .result();
        ItemStack ChatColorExit = new ItemBuilder(Material.BARRIER)
                .setName(CosmeticsCore.color("&c&lBack"))
                .setLore("", CosmeticsCore.color("&bClick here to go back to the main menu!"))
                .result();
        ChatColorGUI.setItem(28, StaffColorHolder);
        ChatColorGUI.setItem(29, StaffColorOrange);
        ChatColorGUI.setItem(30, StaffColorRed);
        ChatColorGUI.setItem(49, ChatColorExit);
        player.openInventory(ChatColorGUI);

    }

}
