package net.chillcraft.cosmeticscore.commands;

import net.chillcraft.cosmeticscore.CosmeticsCore;
import net.chillcraft.cosmeticscore.guis.DropboxGUI;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class DropboxCommand implements CommandExecutor {

    public CosmeticsCore plugin;

    public DropboxCommand(CosmeticsCore plugin) {
        this.plugin = plugin;
    }

    @SuppressWarnings("deprecation")
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("dropbox") && sender instanceof Player) {
            final Player player = (Player) sender;
            DropboxGUI.openMainGUI(plugin, player);
        }
        return true;
    }
}

