package net.chillcraft.cosmeticscore.commands;

import net.chillcraft.cosmeticscore.CosmeticsCore;
import net.chillcraft.cosmeticscore.guis.NameColorGUI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NameColorCommand implements CommandExecutor {

    public CosmeticsCore plugin;

    public NameColorCommand(CosmeticsCore plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("namecolor") && sender instanceof Player) {
            final Player player = (Player) sender;
            NameColorGUI.NameColorInventory(plugin, player);
        }
        return true;
    }
}
