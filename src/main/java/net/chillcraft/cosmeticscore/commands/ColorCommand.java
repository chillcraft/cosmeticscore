package net.chillcraft.cosmeticscore.commands;

import net.chillcraft.cosmeticscore.CosmeticsCore;
import net.chillcraft.cosmeticscore.guis.ColorGUI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ColorCommand implements CommandExecutor{

    public CosmeticsCore plugin;

    public ColorCommand(CosmeticsCore plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("color") && sender instanceof Player){
            final Player player = (Player) sender;
            ColorGUI.OpenMainGUI(plugin, player);
        }
        return true;
    }
}
