package net.chillcraft.cosmeticscore.commands;

import net.chillcraft.cosmeticscore.CosmeticsCore;
import net.chillcraft.cosmeticscore.guis.PerksGUI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PerksCommand implements CommandExecutor {


    public CosmeticsCore plugin;

    public PerksCommand(CosmeticsCore plugin) { this.plugin = plugin; }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
        if (cmd.getName().equalsIgnoreCase("perks") && sender instanceof Player){
            final Player player = (Player) sender;
            PerksGUI.OpenGUI(plugin, player);

        }
    return true;
    }
}
