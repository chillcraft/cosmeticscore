package net.chillcraft.cosmeticscore.util;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * A builder pattern for a dyed {@link ItemStack}.
 *
 * @author frostatronach
 * @see ItemBuilder
 */
@SuppressWarnings({"unused", "deprecation", "WeakerAccess"})
public class DyedItemBuilder extends ItemBuilder {

    public DyedItemBuilder(Material material, DyeColor color) {
        super(new ItemStack(material, 1, color.getData()));
    }

    public DyedItemBuilder(Material material, DyeColor color, int amount) {
        super(new ItemStack(material, amount, color.getData()));
    }

}