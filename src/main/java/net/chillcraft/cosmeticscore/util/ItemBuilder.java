package net.chillcraft.cosmeticscore.util;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A builder pattern for an {@link ItemStack}.
 *
 * @author frostatronach
 */
@SuppressWarnings({"unused", "UnusedReturnValue", "WeakerAccess"})
public class ItemBuilder {

    /**
     * The {@code ItemStack} that's being built
     */
    private ItemStack item;

    /**
     * Build off an existing {@code ItemStack}
     * @param item the existing {@code ItemStack}
     */
    public ItemBuilder(ItemStack item) {
        this.item = item;
    }

    /**
     * Create a new builder from a specified material
     * @param material the item's material
     */
    public ItemBuilder(Material material) {
        this.item = new ItemStack(material);
    }

    /**
     * Build off an existing {@code ItemStack} while
     * specifying an amount
     * @param item the existing {@code ItemStack}
     * @param amount the amount of the new item
     */
    public ItemBuilder(ItemStack item, int amount) {
        this.item = item;
        this.item.setAmount(amount);
    }

    /**
     * Create a new builder from a specified material
     * while specifying an amount
     * @param material the item's material
     * @param amount the item's amount
     */
    public ItemBuilder(Material material, int amount) {
        this.item = new ItemStack(material, amount);
    }

    /**
     * Convert this builder to an {@code ItemStack}.
     * @return this builder as an {@code ItemStack}
     * @see org.bukkit.inventory.ItemStack;
     */
    public ItemStack result() {
        return this.item;
    }

    /*
     * The following are methods to edit the
     * properties of the ItemStack.
     */

    /**
     * Add an enchantment to the item
     *
     * @param enchantment the enchantment to add
     * @param level the level of that enchantment
     * @return a reference to this object
     */
    public ItemBuilder addEnchantment(Enchantment enchantment, int level) {
        item.addUnsafeEnchantment(enchantment, level);
        return this;
    }

    /**
     * Set the item's quantity
     *
     * @param amount the item quantity
     * @return a reference to this object
     */
    public ItemBuilder setAmount(int amount) {
        item.setAmount(amount);
        return this;
    }

    /**
     * Set the material data for the item
     *
     * @param data the material data
     * @return a reference to this object
     */
    public ItemBuilder setData(MaterialData data) {
        item.setData(data);
        return this;
    }

    /**
     * Set the material of the item
     *
     * @param type the item's material
     * @return a reference to this object
     */
    public ItemBuilder setMaterial(Material type) {
        item.setType(type);
        return this;
    }

    /**
     * Set the display name of the item
     *
     * @param name the item's name
     * @return a reference to this object
     */
    public ItemBuilder setName(String name) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
        return this;
    }

    /**
     * Set the lore of the item
     *
     * @param lore the item's lore
     * @return a reference to this object
     */
    public ItemBuilder setLore(List<String> lore) {
        ItemMeta meta = item.getItemMeta();
        meta.setLore(lore);
        item.setItemMeta(meta);
        return this;
    }

    /**
     * Set the lore of the item
     *
     * @param lore the item's lore
     * @return a reference to this object
     */
    public ItemBuilder setLore(String... lore) {
        ItemMeta meta = item.getItemMeta();
        meta.setLore(Arrays.asList(lore));
        item.setItemMeta(meta);
        return this;
    }

    /**
     * Set the lore of the item. The specified color
     * will be added before each line.
     *
     * @param color the color of each line
     * @param lore the item's lore
     * @return a reference to this object
     */
    public ItemBuilder setLore(ChatColor color, String... lore) {
        List<String> newLore = new ArrayList<>(lore.length);
        for (String s : lore) {
            newLore.add(color + s);
        }
        return setLore(newLore);
    }

    /**
     * Set the item to be unbreakable
     *
     * @return a reference to this object
     */
    public ItemBuilder setUnbreakable() {
        ItemMeta meta = item.getItemMeta();
        meta.spigot().setUnbreakable(true);
        item.setItemMeta(meta);
        return this;
    }

}