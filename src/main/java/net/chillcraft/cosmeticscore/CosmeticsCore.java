package net.chillcraft.cosmeticscore;

import net.chillcraft.cosmeticscore.commands.ColorCommand;
import net.chillcraft.cosmeticscore.guis.RankColorGUI;
import net.chillcraft.cosmeticscore.commands.DropboxCommand;
import net.chillcraft.cosmeticscore.listeners.ColorClickListener;
import net.chillcraft.cosmeticscore.listeners.PerksClickListener;
import net.chillcraft.cosmeticscore.listeners.DropboxClickListener;
import net.chillcraft.cosmeticscore.commands.PerksCommand;
import net.chillcraft.cosmeticscore.util.FileManager;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.*;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.plugin.RegisteredServiceProvider;

public class CosmeticsCore extends JavaPlugin{

    private FileManager playerDataManager;
    public static Permission perms = null;
    public static Chat chat;

    public void onEnable() {
    PluginManager manager = this.getServer().getPluginManager();

        this.getConfig();
        this.getConfig().options().copyDefaults(true);
        this.saveDefaultConfig();
        this.getCommand("dropbox").setExecutor(new DropboxCommand(this));
        this.getCommand("perks").setExecutor(new PerksCommand(this));
        this.getCommand("color").setExecutor(new ColorCommand(this));
        this.getCommand("rankcolor").setExecutor(new RankColorGUI(this));
        manager.registerEvents(new DropboxClickListener(this), this);
        manager.registerEvents(new ColorClickListener(this), this);
        manager.registerEvents(new PerksClickListener(this), this);
        setupChat();
        setupPermissions();
    }

    public void onDisable() {
    }


    public FileManager getPlayerData() {
        return this.playerDataManager;
    }

    public boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        chat = rsp.getProvider();
        return chat != null;
    }
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }
    public static Chat getChat() {
        return chat;
    }
    public static Permission getPermissions() {
        return perms;
    }


    public static String color(String text) {
        return ChatColor.translateAlternateColorCodes('&', text);
    }

}
