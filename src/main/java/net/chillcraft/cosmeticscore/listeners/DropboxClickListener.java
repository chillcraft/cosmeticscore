package net.chillcraft.cosmeticscore.listeners;

import net.chillcraft.cosmeticscore.CosmeticsCore;
import net.chillcraft.cosmeticscore.guis.DropboxGUI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.scheduler.BukkitRunnable;


public class DropboxClickListener implements Listener {

    private CosmeticsCore plugin;

    public DropboxClickListener(CosmeticsCore plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {

        if (event.getInventory() != null || event.getClick() != null || event.getCurrentItem() != null) {
            Player player = (Player) event.getWhoClicked();
            String GUIName = plugin.getConfig().getString("GUIName");

            /*
            Dropbox
            */
            if ((event.getInventory().getName()).equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&3&lDropbox"))) {
                event.setCancelled(true);
                if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR || !event.getCurrentItem().hasItemMeta()) {
                    return;
                }
                if ((event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.ENDER_CHEST && event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&b&lMay 2018 Dropbox"))) {
                    if (player.hasPermission("cosmeticscore.commands.may2018")) {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                DropboxGUI.Dropbox1(player);
                            }
                        }.runTaskLater(plugin, 1);
                        return;
                    }
                    if (!player.hasPermission("cosmeticscore.commands.may2018")) {
                        player.sendMessage(ChatColor.RED + "You do not own this commands!");
                        player.closeInventory();
                        return;
                    }
                }
                if ((event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.EMPTY_MAP && event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&b&lMay 2018 Dropbox"))) {
                    player.sendMessage(CosmeticsCore.color("&8&m-----------------------------------------------------"));
                    player.sendMessage(CosmeticsCore.color("&a     Purchase a dropbox at &eshop.chillcraft.net"));
                    player.sendMessage(CosmeticsCore.color("&8&m-----------------------------------------------------"));
                    player.closeInventory();
                    return;
                }
            /*
            May 2018 Dropbox
            */
                if ((event.getInventory().getName()).equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&b&lMay 2018 Dropbox"))) {
                    event.setCancelled(true);
                    if (event.getCurrentItem() == null && event.getCurrentItem().getType() == Material.AIR && !event.getCurrentItem().hasItemMeta()) {
                        return;
                    }
                    if ((event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.NAME_TAG && event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&a[&2Cactus&a] &7Tag"))) {
                        player.performCommand("tags select DBT2M1");
                        return;
                    }
                    if ((event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.NAME_TAG && event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&6[&f*****&6] &7Tag"))) {
                        player.performCommand("tags select DBT1M1");
                        return;
                    }
                    if ((event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.DOUBLE_PLANT && event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&6Rainbow Helix"))) {
                        player.performCommand("h open DropBox.yml");
                        return;
                    }
                    if ((event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.DIAMOND && event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&6Sell Wand Booster - 2x 30 Minutes"))) {
                        player.performCommand("viewavailableboosters");
                        return;
                    }
                    if ((event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.BARRIER && event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&cBack"))) {
                        player.performCommand("commands");
                        return;
                    }
                }
            }
        }
    }
}



