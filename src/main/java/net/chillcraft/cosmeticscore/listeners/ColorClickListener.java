package net.chillcraft.cosmeticscore.listeners;

import net.chillcraft.cosmeticscore.CosmeticsCore;
import net.chillcraft.cosmeticscore.guis.ColorGUI;
import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.Contexts;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.Node;
import me.lucko.luckperms.api.User;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.Map;

public class ColorClickListener implements Listener {

    private LuckPermsApi api = LuckPerms.getApi();
    public CosmeticsCore plugin;

    public ColorClickListener(CosmeticsCore plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {

        if (event.getInventory() != null || event.getClick() != null || event.getCurrentItem() != null) {
            Player player = (Player) event.getWhoClicked();
            String GUIName = plugin.getConfig().getString("GUIName");
            String Prefix = CosmeticsCore.color("&8[&e/color&8] ");

            /*
            Color GUI
            */
            if ((event.getInventory().getName()).equalsIgnoreCase(CosmeticsCore.color("&7/color"))) {
                event.setCancelled(true);
                if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR || !event.getCurrentItem().hasItemMeta()) {
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.NETHER_STAR || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&b&lRank Color"))) {
                    ColorGUI.OpenRankColorGUI(plugin, player);
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.NAME_TAG || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&6&lName Color"))) {
                    ColorGUI.OpenNameColorGUI(plugin, player);
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.GRASS || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&e&lIsland Level Color"))) {
                    ColorGUI.OpenIslandLevelColorGUI(plugin, player);
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.GREEN_RECORD || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&a&lChat Color"))) {
                    ColorGUI.OpenChatColorGUI(plugin, player);
                    return;
                }
            }

            /*
            ================
                  Chat
                 Colors
            ================
             */

            if ((event.getInventory().getName()).equalsIgnoreCase(CosmeticsCore.color("Choose your chat color..."))) {
                event.setCancelled(true);
                if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR || !event.getCurrentItem().hasItemMeta()) {
                    return;
                }

                //Color Information Holders

                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.STAINED_GLASS_PANE || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&fMember Unlocked Colors"))) {
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.STAINED_GLASS_PANE || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&dDonor Unlocked Colors"))) {
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.STAINED_GLASS_PANE || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&cStaff Unlocked Colors"))) {
                    return;
                }

                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&fWhite Chat Color"))) {
                    plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "chatcolor set WHITE " + player.getName());
                    player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &7Gray&7!"));
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&7Gray Chat Color"))) {
                    plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "chatcolor set GRAY " + player.getName());
                    player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &fWhite&7!"));
                    player.closeInventory();
                    return;

                    //Donor Chat Colors

                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&dPink Chat Color"))) {
                    if (player.hasPermission("cosmeticscore.colors.premium")) {
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "chatcolor set LIGHT_PURPLE " + player.getName());
                        player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &dPink&7!"));
                        player.closeInventory();
                    } else if (!player.hasPermission("cosmeticscore.colors.premium")) {
                        player.sendMessage(CosmeticsCore.color("&cYou must be Premium to use this color!"));
                    }
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&aLight Green Chat Color"))) {
                    if (player.hasPermission("cosmeticscore.chatcolor.deluxe")) {
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "chatcolor set GREEN " + player.getName());
                        player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &aLight Green&7!"));
                        player.closeInventory();
                    } else if (!player.hasPermission("cosmeticscore.colors.deluxe")) {
                        player.sendMessage(CosmeticsCore.color("&cYou must be Deluxe to use this color!"));
                    }
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&bLight Blue Chat Color"))) {
                    if (player.hasPermission("cosmeticscore.colors.deluxe")) {
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "chatcolor set BLUE " + player.getName());
                        player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &bLight Blue&7!"));
                        player.closeInventory();
                    } else if (!player.hasPermission("cosmeticscore.colors.deluxe")) {
                        player.sendMessage(CosmeticsCore.color("&cYou must be Deluxe to use this color!"));
                    }
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&eYellow Chat Color"))) {
                    if (player.hasPermission("cosmeticscore.colors.platinum")) {
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "chatcolor set YELLOW " + player.getName());
                        player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &eYellow&7!"));
                        player.closeInventory();
                    } else if (!player.hasPermission("cosmeticscore.colors.platinum")) {
                        player.sendMessage(CosmeticsCore.color("&cYou must be Platinum to use this color!"));
                    }
                    return;
                }

                //Staff Chat Colors

                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&6Gold Chat Color"))) {
                    if (player.hasPermission("cosmeticscore.colors.staff")) {
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "chatcolor set GOLD " + player.getName());
                        player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &6Gold&7!"));
                        player.closeInventory();
                    } else if (!player.hasPermission("cosmeticscore.colors.staff")) {
                        player.sendMessage(CosmeticsCore.color("&cYou must be Staff to use this color!"));
                    }
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&cRed Chat Color"))) {
                    if (player.hasPermission("cosmeticscore.colors.staff")) {
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "chatcolor set RED " + player.getName());
                        player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &cRed&7!"));
                        player.closeInventory();
                    } else if (!player.hasPermission("cosmeticscore.colors.staff")) {
                        player.sendMessage(CosmeticsCore.color("&cYou must be Staff to use this color!"));
                    }

                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.BARRIER || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&c&lBack"))) {
                    player.closeInventory();
                    ColorGUI.OpenMainGUI(plugin, player);
                    return;
                }
            }



            /*
            ==============
                 Name
                Colors
            ==============
            */


            if ((event.getInventory().getName()).equalsIgnoreCase(CosmeticsCore.color("&8Choose your name color..."))) {
                event.setCancelled(true);
                if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR || !event.getCurrentItem().hasItemMeta()) {
                    return;
                }

                //Color Information Holders

                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.STAINED_GLASS_PANE || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&fMember Unlocked Name Colors"))) {
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.STAINED_GLASS_PANE || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&dDonor Unlocked Name Colors"))) {
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.STAINED_GLASS_PANE || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&cStaff Unlocked Name Colors"))) {
                    return;
                }

                //Member Chat Colors

                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&fWhite Name Color"))) {
                    plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "namecolor set WHITE " + player.getName());
                    player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &7White&7!"));
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&7Gray Name Color"))) {
                    plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "namecolor set GRAY " + player.getName());
                    player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &fGray&7!"));
                    player.closeInventory();
                    return;

                    //Donor Chat Colors

                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&dPink Name Color"))) {
                    if (player.hasPermission("cosmeticscore.colors.premium")) {
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "namecolor set LIGHT_PURPLE " + player.getName());
                        player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &dPink&7!"));
                        player.closeInventory();
                    } else if (!player.hasPermission("cosmeticscore.colors.premium")) {
                        player.sendMessage(CosmeticsCore.color("&cYou must be Premium to use this color!"));
                    }
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&aLight Green Name Color"))) {
                    if (player.hasPermission("cosmeticscore.chatcolor.deluxe")) {
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "namecolor set GREEN " + player.getName());
                        player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &aLight Green&7!"));
                        player.closeInventory();
                    } else if (!player.hasPermission("cosmeticscore.colors.deluxe")) {
                        player.sendMessage(CosmeticsCore.color("&cYou must be Deluxe to use this color!"));
                    }
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&bLight Blue Name Color"))) {
                    if (player.hasPermission("cosmeticscore.colors.deluxe")) {
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "namecolor set BLUE " + player.getName());
                        player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &bLight Blue&7!"));
                        player.closeInventory();
                    } else if (!player.hasPermission("cosmeticscore.colors.deluxe")) {
                        player.sendMessage(CosmeticsCore.color("&cYou must be Deluxe to use this color!"));
                    }
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&eYellow Name Color"))) {
                    if (player.hasPermission("cosmeticscore.colors.platinum")) {
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "namecolor set YELLOW " + player.getName());
                        player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &eYellow&7!"));
                        player.closeInventory();
                    } else if (!player.hasPermission("cosmeticscore.colors.platinum")) {
                        player.sendMessage(CosmeticsCore.color("&cYou must be Platinum to use this color!"));
                    }
                    return;
                }

                //Staff Chat Colors

                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&6Gold Name Color"))) {
                    if (player.hasPermission("cosmeticscore.colors.staff")) {
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "namecolor set GOLD " + player.getName());
                        player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &6Gold&7!"));
                        player.closeInventory();
                    } else if (!player.hasPermission("cosmeticscore.colors.staff")) {
                        player.sendMessage(CosmeticsCore.color("&cYou must be Staff to use this color!"));
                    }
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.WOOL || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&cRed Name Color"))) {
                    if (player.hasPermission("cosmeticscore.colors.staff")) {
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "namecolor set RED " + player.getName());
                        player.sendMessage(Prefix + CosmeticsCore.color("&7Your chat color is now &cRed&7!"));
                        player.closeInventory();
                    } else if (!player.hasPermission("cosmeticscore.colors.staff")) {
                        player.sendMessage(CosmeticsCore.color("&cYou must be Staff to use this color!"));
                    }

                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.BARRIER || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&c&lBack"))) {
                    player.closeInventory();
                    ColorGUI.OpenMainGUI(plugin, player);
                    return;
                }
            }



            /*
            ==============
                 Rank
                Colors
            ==============
            */
            String sender = player.getName();
            User user = api.getUser(player.getUniqueId());
            if ((event.getInventory().getName()).equalsIgnoreCase(CosmeticsCore.color("&8Choose your rank color..."))) {
                event.setCancelled(true);
                if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR && !event.getCurrentItem().hasItemMeta()) {
                    return;
                }
                if (event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.WOOL && event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&cRed Rank Color"))) {
                    Node redprefix = getNodeFor(api, 5, "&c&lPlatinum");
                    user.setPermission(redprefix);
                    player.closeInventory();
                    player.sendMessage(ChatColor.GRAY + "Your prefix has been set!");
                    return;
                }
                if (event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.WOOL && event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&6Gold Rank Color"))) {
                    Node goldprefix = getNodeFor(api, 5, "&6&lPlatinum");
                    user.setPermission(goldprefix);
                    player.closeInventory();
                    player.sendMessage(ChatColor.GRAY + "Your prefix has been set!");
                    return;
                }
                if (event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.WOOL && event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&eYellow Rank Color"))) {
                    Node yellowprefix = getNodeFor(api, 5, "&e&lPlatinum");
                    user.setPermission(yellowprefix);
                    player.closeInventory();
                    player.sendMessage(ChatColor.GRAY + "Your prefix has been set!");
                    return;
                }
                if (event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.WOOL && event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&bLight Blue Rank Color"))) {
                    Node lightblueprefix = getNodeFor(api, 5, "&b&lPlatinum");
                    user.setPermission(lightblueprefix);
                    player.closeInventory();
                    player.sendMessage(ChatColor.GRAY + "Your prefix has been set!");
                    return;
                }
                if (event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.WOOL && event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&dPink Rank Color"))) {
                    Node pinkprefix = getNodeFor(api, 5, "&d&lPlatinum");
                    user.setPermission(pinkprefix);
                    player.closeInventory();
                    player.sendMessage(ChatColor.GRAY + "Your prefix has been set!");
                    return;
                }
                if (event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.BARRIER && event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&c&lReset Rank Color"))) {
                    for (Map.Entry<Integer, String> prefix : user.getCachedData().getMetaData(Contexts.global()).getPrefixes().entrySet()) {
                        user.unsetPermission(api.getNodeFactory().makePrefixNode(prefix.getKey(), prefix.getValue()).build());
                    }
                    player.closeInventory();
                    player.sendMessage(ChatColor.GRAY + "Your prefix has been reset!");

                    return;
                }
            /*
            ==============
                Island
                 Level
                Colors
            ==============
             */


            }
        }
    }
    private Node getNodeFor(LuckPermsApi api, int weight, String content) {
        return api.getNodeFactory()
                .makePrefixNode(weight, "&8[" + content + "&8]&r ")
                .build();
    }
}