package net.chillcraft.cosmeticscore.listeners;

import net.chillcraft.cosmeticscore.CosmeticsCore;
import me.lucko.luckperms.LuckPerms;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import me.lucko.luckperms.api.*;

public class PerksClickListener implements Listener {

    private CosmeticsCore plugin;
    public PerksClickListener(CosmeticsCore plugin) { this.plugin = plugin; }
    LuckPermsApi api = LuckPerms.getApi();


    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {

        if (event.getInventory() != null || event.getClick() != null || event.getCurrentItem() != null) {
            Player player = (Player) event.getWhoClicked();
            String GUIName = plugin.getConfig().getString("GUIName");
            String Prefix = CosmeticsCore.color("");
            String group = CosmeticsCore.perms.getPrimaryGroup(player);

            /*
            Perks Click Event
            */
            if ((event.getInventory().getName()).equalsIgnoreCase(CosmeticsCore.color("&8Donor Rank Perks"))) {
                event.setCancelled(true);
                if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR || !event.getCurrentItem().hasItemMeta()) {
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.STAINED_GLASS || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&6Premium &e(&a$15.00&e)"))) {
                    player.sendMessage(CosmeticsCore.color("&8&m-----------------------------------------------------"));
                    player.sendMessage(CosmeticsCore.color("&a     Purchase ranks and boosters at &eshop.chillcraft.net"));
                    player.sendMessage(CosmeticsCore.color("&8&m-----------------------------------------------------"));
                    player.closeInventory();
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.STAINED_GLASS || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&5Deluxe &d(&a$25.00&d)"))) {
                    player.sendMessage(CosmeticsCore.color("&8&m-----------------------------------------------------"));
                    player.sendMessage(CosmeticsCore.color("&a     Purchase ranks and boosters at &eshop.chillcraft.net"));
                    player.sendMessage(CosmeticsCore.color("&8&m-----------------------------------------------------"));
                    player.closeInventory();
                    return;
                }
                if ((event.getCurrentItem() != null || event.getCurrentItem().getType() == Material.STAINED_GLASS || event.getCurrentItem().hasItemMeta()) && event.getCurrentItem().getItemMeta().getDisplayName().equals(CosmeticsCore.color("&7&lPlatinum &f(&a$50.00&f)"))) {
                    player.sendMessage(CosmeticsCore.color("&8&m-----------------------------------------------------"));
                    player.sendMessage(CosmeticsCore.color("&a     Purchase ranks and boosters at &eshop.chillcraft.net"));
                    player.sendMessage(CosmeticsCore.color("&8&m-----------------------------------------------------"));
                    player.closeInventory();
                    return;
                }
            }
        }
    }
}


